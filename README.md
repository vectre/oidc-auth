# Swansea University Computer Society OIDC Provider

## What is OIDC and what is it providing?
OpenID Connect is essentially Oauth with extra bits. That's the super simple way of explaining it. You get a token, just like in Oauth, but this one is a JSON Web Token which means that it can contain profile info, roles, etc.

The provider is the thing that generates these tokens. In our case, we're using Panva's OIDC Provider library for Node.js. That's because I like Javascript - but to be honest, you could use anything.

We're also using an LDAP backend to find the users and their roles, which is then ported in to the OIDC Provider.

And that's basically it...for now, until I find issues along the way and have to change things.
